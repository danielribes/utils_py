#!/usr/bin/env python
#-*- coding: utf-8 -*-
# Llista un fitxer numerant línia per línia
# Daniel Ribes
#

import sys

if len(sys.argv) != 2:
    sys.exit("Cal utilitzar: nl.py nomfitxer")

elfitxer = sys.argv[1]
comptador = 1

fitxer = open(elfitxer, "r")
while True:
    unalinia = fitxer.readline()
    if not unalinia: 
        break
    print '%05d | %s' % (comptador,unalinia.rstrip())
    comptador = comptador + 1
fitxer.close()
print "\n",comptador-1," linies.\n"
